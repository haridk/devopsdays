# This class installs Nginx and class the appropriate
# sub-class based on Operating System name.
#
# @summary Install and start Nginx
#
# @api private
#
# @see devopsdays
# @see devopsdays::nginx::ubuntu
# @see devopsdays::nginx::el
# @see https://forge.puppet.com/puppet/nginx Nginx Module
# @see https://nginx.org/en/docs/ Nginx Docs
#
# @author David Hollinger <david.hollinger@moduletux.com>
#
class devopsdays::nginx {
  assert_private()

  class { 'nginx':
    confd_purge  => true,
    server_purge => true,
  }

  case $facts['os']['name'] {
    'Ubuntu': { contain devopsdays::nginx::ubuntu }
    /^(RedHat|CentOS)$/: { contain devopsdays::nginx::el }
    default: { fail("${facts['os']['name']} is not supported!") }
  }
}
